import React from "react";
import styles from "css";
export function Label(props) {
  return React.createElement("span", { ...props, className: styles.Label
  });
}
