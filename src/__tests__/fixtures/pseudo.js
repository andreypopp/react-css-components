import React from "react";
import styles from "css";
export function Label(props) {
  return React.createElement("div", { ...props, className: styles.Label
  });
}
